import functools
import sys
from contextlib import contextmanager

def stderr_redirect(dest=None):
    def dec(func):
        functools.wraps(func)
        def wrapper(*args, **kwargs):
            with manager(dest):
                return func(*args, **kwargs)
        return wrapper
    return dec
     
@contextmanager
def manager(dest=None):
    dest = dest or sys.stdout
    old = sys.stderr
    sys.stderr = open(dest, 'a+') if dest or sys.stderr.name != sys.__stderr__.name else sys.stderr
    try:
        yield
    finally:
        sys.stderr = old

        
@stderr_redirect(dest='result.txt')
def against_rules():
    a = 10/0
    return a

@stderr_redirect(dest='result.txt')
def test():
    sys.stderr.write('test\n')
    return

test()
against_rules()