from os import getpid, stat
import time

class Pid_Exception(Exception):
    pass

class pidfile:
    Process_ID = getpid()
    
    def __init__(self, file_name: str):
        self.file = None
        self.name = file_name
        

    def __enter__(self):
        if os.stat(self.name).st_size != 0:
            self.file = open(self.name, 'r')
            try:
                prev_pid = int(self.file.readline())
            except:
                raise ValueError('Pidfile is wrong!')
            try:
                os.kill(prev_pid, 0)
            except OSError:
                pass
            else:
                self.file.close()
                raise Pid_Exception('Existing Pidfile!')

        self.file = open(self.name, 'w')
        self.file.write(str(self.Process_ID))
        self.file.flush()
        os.fsync(self.file.fileno())
        
'''The method fsync() forces write of file with file descriptor fd to disk. 
If you're starting with a Python file object f, 
first do f.flush(), and then do os.fsync(f.fileno()), 
to ensure that all internal buffers associated with f are written to disk.

f.fileno() -  the file descriptor for buffer sync'''

    def __exit__(self, *exc_info):
        self.file = open(self.name, 'w')
        self.file.close()
        if exc_info:
            print(exc_info)


with pidfile('file1'):
    time.sleep(60)

with pidfile('file1'):
    time.sleep(60)